import type { NextPage } from "next";


const Register: NextPage = ({}) => {
  return (
    <div className="login-page">
      <div className="container mx-auto px-3">
        <div className="flex flex-wrap justify-center items-center h-screen pb-40 -mx-3">
          <div className="px-3 grow md:grow-0 md:basis-10/12 lg:basis-6/12">
            <div
              className="bg-gray-100 shadow-lg shadow-gray-400 rounded-lg md:px-8 py-6 px-5 mb-4">
              <div className="mb-4">
                <div>
                  <label className="block text-grey-darker text-sm font-medium mb-2" htmlFor="username">
                    Username
                  </label>
                  <input
                    type="text"
                    name="username"
                    id="username"
                    className="focus:ring-indigo-500 focus:border-indigo-500 block w-full px-3 py-2 sm:text-sm border-gray-300 rounded-md"
                  />
                </div>
              </div>
              <div className="mb-6">
                <label className="block text-grey-darker text-sm font-medium mb-2" htmlFor="password">
                  Password
                </label>
                <input
                  className="focus:ring-indigo-500 focus:border-indigo-500 block w-full px-3 py-2 sm:text-sm border-gray-300 rounded-md mb-2"
                  id="password" type="password" />
              </div>
              <div className="flex items-center justify-between">
                <button
                  className="bg-green-800 hover:bg-blue-dark text-white font-medium py-2 px-4 rounded"
                  type="button">
                  Create new account
                </button>
                <a
                  className="inline-block align-baseline font-normal underline text-sm text-neutral-900 hover:text-blue-darker"
                  href="#">
                  Forgot Password?
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};


export default Register;
