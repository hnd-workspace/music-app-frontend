import type { NextPage } from "next";
import Header from "../src/components/commons/Header";
import PlayBar from "../src/components/commons/PlayBar";
import Queue from "../src/components/index/Queue";
import Search from "../src/components/index/Search";
import Thumbnail from "../src/components/index/Thumbnail";

export const getServerSideProps = async (context) => {
  return {
    props: {},
  };
};

const Home: NextPage = ({}) => {
  // khi nó re render
  return (
    <div className="home-page">
      <Header />
      <PlayBar />
      <Thumbnail />
      <Queue />
      <Search />
    </div>
  );
};


export default Home;
