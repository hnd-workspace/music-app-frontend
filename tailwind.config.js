module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
    fontFamily: {
      "sans": [ "Poppins", "Helvetica", "Arial", "ui-sans-serif", "sans-serif" ],
    }
  },
  plugins: [],
};
