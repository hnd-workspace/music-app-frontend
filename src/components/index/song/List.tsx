import Item from "./Item";

const List = () => {
  return <div>
    {
      new Array(5).map((i, index) => {
        return <Item key={ index } />;
      })
    }
  </div>;
};

export default List;