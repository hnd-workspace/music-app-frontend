import { SET_LOADING } from "../../store/app";
import { useAppDispatch } from "../../store/hooks";
import List from "./song/List";

const Search = () => {
  const dispatch = useAppDispatch();

  const setLoading = () => {
  };

  return (
    <div>
      <input type="text" />
      <button onClick={ setLoading } />
      <hr />
      <h3>Searched music here</h3>
      <List />
      <hr />
    </div>
  );
};


export default Search;