import axiosInstance from "./api";

const getSongFromAPI = (url: string) => {
  return axiosInstance.get(url);
};