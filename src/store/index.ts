import {
  Action,
  configureStore,
  ThunkAction,
} from "@reduxjs/toolkit";
import app from "./app";
import song from "./song";
import auth from "./auth";

export const store = configureStore({
  reducer: {
    app,
    auth,
    song
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType,
  RootState,
  unknown,
  Action<string>>;