import { createSlice } from "@reduxjs/toolkit";

const songSlice = createSlice({
  reducers: {},
  extraReducers: {},
  initialState: {},
  name: "song"
});

export default songSlice.reducer;