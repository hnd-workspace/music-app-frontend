import { createSlice } from "@reduxjs/toolkit";

const authSlice = createSlice({
  reducers: {},
  extraReducers: {},
  initialState: {},
  name: "auth"
});

export default authSlice.reducer;