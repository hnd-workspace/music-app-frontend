import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const appSlice = createSlice({
  name: "app",
  // State => data
  initialState: {
    loading: false,
  },
  // actions => reducers
  reducers: {
    SET_LOADING: (state, payload: PayloadAction<boolean>) => {
      state.loading = payload.payload;
    }
  },
});

export const { SET_LOADING } = appSlice.actions;
export default appSlice.reducer;